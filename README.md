Translation of block world game actions expressed in natural language into ASP facts for resolution planning.

Authors:

- Stefano Valentini https://gitlab.com/valent0ne
- Valentina Cecchini https://gitlab.com/Nimerya
